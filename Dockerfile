FROM debian:stable
MAINTAINER Gus <gus@torproject.org>

ENV APP="signalbridgesbot"
ENV APP_BASE="/srv"
ENV SHELL="/bin/bash"
ENV PYTHONPATH="/usr/local/share/semaphore"

ENV SIGNAL_PHONE_NUMBER="${SIGNAL_PHONE_NUMBER:-+00000000000}"

RUN apt-get update && \
    apt-get install -y \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*

RUN pip install semaphore-bot --upgrade -t ${PYTHONPATH}

## Regular user support: to get things right, needs to solve
## the sharing of a socket between this container service and
## signald's.
##
## UID and GID might be read-only values, so use non-conflicting ones
## They're used to make sure that the container service use tha same
## UID and GID from the user running docker. This is needed in case
## the container service might read and write data in shared folders.
#ARG CONTAINER_UID="${CONTAINER_UID:-1000}"
#ARG CONTAINER_GID="${CONTAINER_GID:-1000}"
#
## Create a non-privileged user to run the service
#RUN mkdir -p ${APP_BASE}/${APP} /home/${APP}
#RUN groupadd -r -g ${CONTAINER_GID} ${APP} && \
#    useradd --no-log-init -r -u ${CONTAINER_UID} -g ${APP} ${APP} && \
#    mkdir -p /home/${APP} && chown ${APP}. /home/${APP}
#RUN chown -R ${APP}.${APP} ${APP_BASE}/${APP}
#
#USER ${APP}

WORKDIR ${APP_BASE}/${APP}

COPY . ${APP_BASE}/${APP}

ENTRYPOINT exec /usr/bin/python3 ${APP_BASE}/${APP}/signalbridgesbot.py
